const path = require('path');

module.exports = {
    mode: 'development',
    target: 'web',
    entry: './src/js/index.js',
    output: {
        filename: 'custom.js',
        path: path.resolve(__dirname, './assets')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    }
};