let config = require('./config.json');
try {
    const localConfig = require('./config.local.json');
    config = Object.assign(config, localConfig);
} catch (ignored) {}
const gulp = require('gulp');
const concat = require('gulp-concat');
const browserSync = require('browser-sync').create();
const themeKit = require('@shopify/themekit');
const argv = require('yargs').argv;
const open = require('gulp-open');
const webpack = require('webpack');
const webpackConfigDev = require('./webpack.config.dev');
const webpackConfigProd = require('./webpack.config.prod');
const tap = require('gulp-tap');
const path = require('path');
const replace = require('gulp-replace');
const svgo = require('gulp-svgo');
const rename = require('gulp-rename');

let env = process.env.NODE_ENV || 'development';
const src = './src';

gulp.task('set-dev-env', function(cb) {
   env = 'development';
   cb();
});

gulp.task('set-prod-env', function(cb) {
   env = 'production';
   cb();
});

gulp.task('svgs', function() {
  return gulp.src('src/svgs/*')
      .pipe(svgo({
        plugins: [
          {
            removeViewBox: false
          }
        ]
      }))
      .pipe(rename(function (path) {
          return {
              dirname: path.dirname,
              basename: 'icon-' + path.basename + '.svg',
              extname: '.liquid'
          };
      }))
      .pipe(gulp.dest('snippets/'));
});

gulp.task('scss', function() {
    return gulp.src(['./assets/styles.scss.liquid',
        `${src}/scss/mixins/**/*.scss.liquid`,
        `${src}/scss/variables/**/*.scss.liquid`,
        `${src}/scss/base/**/*.scss.liquid`,
        `${src}/scss/ui-components/**/*.scss.liquid`,
        `${src}/scss/components/**/*.scss.liquid`,
        `${src}/scss/sections/**/*.scss.liquid`,
        `${src}/scss/pages/**/*.scss.liquid`,
        `${src}/scss/vendor/**/*.scss.liquid`,
        `${src}/scss/utilities/**/*.scss.liquid`])
        .pipe(concat('styles-customised.scss.liquid'))
        .pipe(gulp.dest('./assets'));
});

gulp.task('stylelint', function lintCssTask() {
  const gulpStylelint = require('gulp-stylelint');

  return gulp
    .src('src/scss/**/*.liquid')
    .pipe(gulpStylelint({
      syntax: 'scss',
      fix: true,
      reporters: [
        {
          formatter: 'string',
          console: true
        },
        {
          formatter: 'string',
          save: 'stylelint-report.txt'
        }
      ]
    }));
});

gulp.task('js', function() {
    const webpackConfig = env === 'development' ? webpackConfigDev : webpackConfigProd;
    return new Promise((resolve, reject) => {
       webpack(webpackConfig, (error, stats) => {
           if (error) {
               return reject(error);
           }
           if (stats.hasErrors()) {
               return reject(new Error(stats.compilation.errors.join('\n')));
           }
           resolve();
       });
    });
});

const templates = [
    '**/*.liquid',
    '!./assets/**/*',
    '!./node_modules/**/*',
    `!${src}/**/*`,
    '!./snippets/customer-has-tag.liquid',
    '!./snippets/customer-is-wholesale.liquid',
    '!./snippets/product-properties.liquid',
    '!./snippets/product-property.liquid',
    '!./snippets/product-has-tag.liquid',
    '!./snippets/titlecase.liquid',
    '!./snippets/cart-invalid-quantity.liquid'];

gulp.task('add-template-name', function() {
    return gulp.src(templates)
        .pipe(tap(function(file) {
            const filePath = path.relative(path.resolve('./'), file.path);
            const regex = /^<!-- Template:/;
            if (!regex.test(file.contents)) {
                file.contents = Buffer.concat([
                    new Buffer.from(`<!-- Template: ${filePath} -->\n`), file.contents
                ]);
            }
        }))
        .pipe(gulp.dest('./'));
});

gulp.task('remove-template-name', function() {
    return gulp.src(templates)
        .pipe(replace(/<!-- Template: .+ -->\n/, ''))
        .pipe(gulp.dest('./'));
});

gulp.task('watch-dev', gulp.series(['set-dev-env', function() {
    gulp.watch(`${src}/scss/**/*.scss.liquid`, gulp.series('scss', 'stylelint'));
    gulp.watch(`${src}/js/**/*.js`, gulp.series('js'));
    gulp.watch(`${src}/svgs/*.svg`, gulp.series('svgs'));

    themeKit.command('watch', {
        notify: '/tmp/theme.update',
        env: 'development',
        vars: 'themekit-vars'
    });

    browserSync.init({
        proxy: `${config.storeUrl}${config.path}?preview_theme_id=${config.themeId.development}`,
        reloadDelay: 3000
    });

    gulp.watch('/tmp/theme.update').on('change', browserSync.reload);
}]));

gulp.task('watch-production', gulp.series(['set-prod-env', function() {
    gulp.watch(`${src}/scss/**/*.scss.liquid`, gulp.series('scss', 'stylelint'));
    gulp.watch(`${src}/js/**/*.js`, gulp.series('js'));
    gulp.watch(`${src}/svgs/*.svg`, gulp.series('svgs'));

    themeKit.command('watch', {
        notify: '/tmp/theme.update',
        env: 'production',
        vars: 'themekit-vars'
    });

    browserSync.init({
        proxy: `${config.storeUrl}${config.path}?preview_theme_id=${config.themeId.production}`,
        reloadDelay: 3000
    });

    gulp.watch('/tmp/theme.update').on('change', browserSync.reload);
}]));

gulp.task('deploy', function() {
    return themeKit.command('deploy', {
        env: env,
        vars: 'themekit-vars'
    });
});

gulp.task('deploy-dev', gulp.series(['set-dev-env', 'scss', 'js', 'deploy']));
gulp.task('deploy-prod', gulp.series(['set-prod-env', 'scss', 'js', 'deploy']));
gulp.task('deploy-both', gulp.series('deploy-dev', 'deploy-prod'));

gulp.task('download', function() {
    return themeKit.command('download', {
        env: env,
        vars: 'themekit-vars'
    });
});

gulp.task('download-dev', gulp.series(['set-dev-env', 'download']));
gulp.task('download-prod', gulp.series(['set-prod-env', 'download']));

gulp.task('download-theme', gulp.series(['set-dev-env', function(cb) {
    if (argv.themeid == null) {
        console.log('Argument --themeid is required');
        cb();
    } else {
        return themeKit.command('deploy', {
            env: env,
            vars: 'themekit-vars',
            themeid: argv.themeid
        });
    }
}]));

gulp.task('preview', function() {
   const themeId = config.themeId[env];
    gulp.src(__filename)
        .pipe(open({uri: `${config.storeUrl}${config.path}?preview_theme_id=${themeId}`}))
});

gulp.task('preview-dev', gulp.series(['set-dev-env', 'preview']));
gulp.task('preview-prod', gulp.series(['set-prod-env', 'preview']));

gulp.task('default', gulp.series('watch-dev'));