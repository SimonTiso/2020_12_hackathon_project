## Setup

* [Install ThemeKit](https://shopify.github.io/themekit/)
* Install gulp with `npm install --global gulp-cli`
* Run ` sh app/bin/setup.sh` from the webroot which will do the following: 
    * Install the node modules with `npm install`
    * Copy `themekit-vars-example` to `themekit-vars` and **fill in the password**.
        
 * Add the *password* when you login into the store under app -> "manage private apps" -> "ThemeKit" -> password under "Admin API".

## Change logs

- Any major changes to the out of the sandbox theme, should be logged in "THEME-CHANGELOG.md" and general changes added into "CHANGELOG.md".
- Theme changes is anything that alters (e.g. see megamenu) or new configuration (e.g. mobile banner) where as changelog is a rough idea of work done on that date so we have a record.



## Dev Tools

- To be refactored into a git hooks plugin at some point.

- **stylelint** - `npm run stylelint` using <https://www.npmjs.com/package/stylelint-config-recommended>
Please note that this will most likely for this project be added to bitbucket pipelines and you will need to pass builds to commit to master.


## Development

**Note** You can run either `sh app/bin/dev-watch.sh` or `sh app/bin/prod-watch.sh` if you want to run dev or prod env with gulp watch.

There is the concept of production and development themes. During development you will be editing the development theme.

Before starting to work on the theme type:

    gulp watch
    
When you run this the first time, Chrome will present a warning saying that "Your connection is not private". Press the Advanced button and then click on "Proceed to https://localhost:3000". When you make changes, files will be uploaded to the development theme and the browser will be refreshed.

When you access a collection page the Filter app redirects to the original version without the filters. To get around this use the store domain instead of localhost:3000 and refresh the browser manually.

When you're finished development, commit and push your changes to the repo. It's a good idea to have a commit per Teamwork task so you can undo it later if needed. Prefix the commit message with the task ID and what you changed e.g. 11431777: Changed product tile on collections page.
    
### Templates

Sometimes it can be difficult to figure out which template a piece of HTML is in so you can add the name of the template as a HTML comment with:

    gulp add-template-name
    
Something like the following will be added to the top of the template:

    <!-- Template: sections/article-template.liquid -->
       
which you can view in Chrome dev tools. Files that you don't want to add a comment to should be added to the templates array in `gulpfile.js` prefixed with `!`. You should remove the comments before pushing to production with:

    gulp remove-template-name 
    
### SCSS

Add your custom SCSS in separate files in `src/scss`. They will get added to the base theme's `styles.scss.liquid` to generate `styles-customised.scss.liquid`. Only files with extension `.scss.liquid` will be added. I've used the following directory structure in `src/scss`:

- utilities: mixins and functions
- variables: global variables and colours
- base: global styles
- components: components smaller than a section, e.g. breadcrumbs
- sections: global section e.g. header
- pages: a directory for each page and a file for each section in the page

Gulp will add files in the above order. Generally I try to break the SCSS into files that correspond to Shopify sections. Sections that only appear on one page can go into `pages/<page name>/` while sections that are used throughout the site go in `sections/`. If a section has a lot of SCSS you could break it down into separate files and put them in a folder e.g. `sections/header/`. Chunks of SCSS that are smaller than a section go in `components/` and global styles would go in `base/`. `.scss.liquid` files can contain Liquid templating. 

### JavaScript

JavaScript files should be added to `src/js`. Webpack and Babel is used to compile the JavaScript so you can use ES6 modules and ES6+ syntax. Create a JavaScript file per feature and import and call it in `index.js`. The generated file is `assets/custom.js` and needs to be imported into the template with a script tag in `layout/theme.liquid` and `layout/password.liquid`. With this setup JavaScript files cannot contain Liquid templating. If you want to use Liquid templating create a file with the extension `.js.liquid` and put it in the `assets` directory and import it into the template with a script tag. You won't be able to use a transpiler with these files as they won't contain valid JavaScript.
    
## Updating the theme with Shopify apps

When you need a Shopify app to modify a theme, you should modify the development theme. Make sure it is up to date before doing this with:

    gulp deploy-dev
    
If the app duplicates the theme you need to get the ID of the duplicate theme by clicking on Actions > Edit Code. The ID is the last number in the URL. Then type:
 
    gulp download-theme --themeid=<theme ID>
    
If the app doesn't duplicate the theme you can download it with:

    gulp download-dev
    
If there are conflicts the updater app will list the files. In Source Tree look at the changes in these files and remove changes that overwrite your changes. Then commit the changes.

Update the development theme with:

    gulp deploy-dev

And preview it with:

    gulp preview-dev
    
If it looks ok, update the production theme with:

    gulp deploy-prod

## Local and remote versions get out of sync

If the local and remote versions of the themes get out of sync you can run one of the following:

    gulp download-dev
    gulp download-prod

    gulp deploy-dev
    gulp deploy-prod
    gulp deploy-both
    
After downloading the theme you should create a commit for the remote changes.
